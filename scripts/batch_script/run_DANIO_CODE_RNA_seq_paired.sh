#!/bin/bash
# Run this script locally
# You must be logged in to your DNAnexus project 
# USAGE: ./run_DANIO_CODE_RNA_seq_paired.sh /pilot_test/RNA-seq/DCD006425BS/ DNAnexus
set -x
tmp="files.tmp"
path=$1					#Path of the files to run
out=$2					#Ouptut folder into the DNAneuxs platform
opts="-y --brief"
dx ls ${path}*1.fastq.gz | sort > r1.tmp
dx ls ${path}*2.fastq.gz | sort > r2.tmp
dx mkdir ${path}${out}
paste r1.tmp r2.tmp > $tmp
#call the workflow
workflow=project-Byfqfp80G62z08VgfZ96gKVq:workflow-Bzg9Fg80G62xz17GV10jQ6pk
#Launch workflow
while read -r a b; do 
   name=`echo $a | cut -d'.' -f1-3`;  
   echo dx run $workflow -i0.fwd_reads=$path$a -i0.rev_reads=$path$b $opts --folder=${path}${out}/${name}; 
done < $tmp
rm *tmp
