## What does this app do?

This app generate the reference genome index for mapping using STAR (Spliced Transcripts Alignment to a Reference, version 2.5.1b).


## What are typical use cases for this app?

This app can be used to generate index file for a reference genome. Indexing is an one-time operation that is necessary for the **STAR_Mapping** app to map FASTQ files to the genome. 


## What data are required for this app to run?

This app requires reference genome sequence in FASTA format (`fa.gz`). A genome annotation file in GTF format (`*.gtf`) is optional (but recommended) for this app. If you will be utilizing the *--sjdbGTFfile* option in the **STAR_Mapping** app, the annotation file in GTF (`*.gtf`) format is required.

## What does this app output?
This app outputs the indexed genome in a tarball (`*.tar.gz`) consisting in a directory named `genome/`. The compressed diretory contains the following files:

`chrLength.txt`  
`chrNameLength.txt`  
`chrName.txt`  
`chrStart.txt`  
`Genome`  
`genomeParameters.txt`  
`SA`  
`SAindex`  
`transcriptInfo.tab` (if the genome annotation file is provided)


## How does this app work?
This app performs the STAR software package with option `--runMode genomeGenerate`. The mode of generating index can be adjusted with following options (parameters), while the basic options (listed below) are wrapped.
`--runThreadN NumberOfThreads`
`--genomeDir /path/to/genomeDir`
`--genomeFastaFiles /path/to/genome/fasta1 /path/to/genome/fasta2 ...`
`--sjdbGTFfile /path/to/annotations.gtf`
`--sjdbOverhang ReadLength-1`


STAR is an ultrafast universal RNA-seq aligner. For general information, consult the STAR manual at:


https://github.com/alexdobin/STAR/blob/master/doc/STARmanual.pdf


For details, please see the STAR paper:
> A. Dobin et al, STAR: ultrafast universal RNA-seq aligner (2012, Bioinformatics) [DOI] (http://doi.org/10.1093/bioinformatics/bts635)
 
